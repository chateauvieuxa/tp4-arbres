

import java.util.*;


public class Test {
    public static void printTest(String msg, boolean b){
        System.out.println("test : "+(b?" ok":" KO") + " (" + msg + ")");
    }

    public static ABR construitArbre0(){
        ABR a= new ABR();
        a.setFilsD(new ABR(10,new ABR(),new ABR()));
        a.setFilsG(new ABR());
        a.setVal(5);
        return a;
    }

    public static ABR construitArbre1(){
        ABR a= new ABR();
        a.insert(10);
        a.insert(5);
        a.insert(1);
        a.insert(20);
        a.insert(15);
        a.insert(30);
        a.insert(40);
        a.insert(8);
        return a;
    }

    public static boolean contientTous(ABR a, int[] t){
        boolean res = true;
        int i=0;
        while(res && i < t.length){
            res = res && a.recherche(t[i]);
            i++;
        }
        return res;
    }
    public static void testRecherche(){
        ABR a = construitArbre1();
        printTest("testRecherche1", a.recherche(20));
        printTest("testRecherche2", a.recherche(8));
        printTest("testRecherche3", !a.recherche(0));
        printTest("testRecherche4", !a.recherche(16));
    }

    public static void testEgal1(){
        ABR a = new ABR();
        ABR ag = new ABR();
        ag.setVal(1);
        ag.setFilsD(new ABR());
        ag.setFilsG(new ABR());
        a.setVal(5);
        a.setFilsD(new ABR());
        a.setFilsG(ag);


        ABR a2 = new ABR();
        ABR ag2 = new ABR();
        ag2.setVal(1);
        ag2.setFilsD(new ABR());
        ag2.setFilsG(new ABR());
        a2.setVal(5);
        a2.setFilsD(new ABR());
        a2.setFilsG(ag);

        printTest("testEgal1",a.egal(a2));
    }

    public static void testEgal2(){
        ABR a = new ABR();
        ABR ag = new ABR();
        ag.setVal(1);
        ag.setFilsD(new ABR());
        ag.setFilsG(new ABR());
        a.setVal(5);
        a.setFilsD(new ABR());
        a.setFilsG(ag);
        ABR a2 = new ABR();
        printTest("testEgal2",!a.egal(a2));
    }

    public static void testEgal3(){
        ABR a = new ABR();
        ABR ag = new ABR();
        ag.setVal(1);
        ag.setFilsD(new ABR());
        ag.setFilsG(new ABR());
        a.setVal(5);
        a.setFilsD(new ABR());
        a.setFilsG(ag);
        ABR a2 = new ABR();
        printTest("testEgal3",!a2.egal(a));
    }



    public static void testEgal4(){
        ABR a = construitArbre1();
        ABR a2 = construitArbre1();
        printTest("testEgal4",a.egal(a2));
    }

    public static void testEgal5(){
        ABR a = construitArbre1();
        ABR a2 = new ABR();
        printTest("testEgal5",! a.egal(a2) && !a2.egal(a));
    }


    public static void testSuppr1() {
        ABR a = construitArbre1();
        a.suppr(1);
        int[] set = {5,8,10,15,20,30,40};
        printTest("testsuppr1", !a.recherche(1) && contientTous(a,set));
    }

    public static void testSuppr2() {
        ABR a = construitArbre1();
        a.suppr(10);
        int[] set = {1,5,8,15,20,30,40};
        printTest("testsuppr2", !a.recherche(10) && contientTous(a,set));
    }
    public static void testSuppr3() {
        ABR a = construitArbre1();
        a.suppr(20);
        int[] set = {1,5,8,10,15,30,40};
        printTest("testsuppr3", !a.recherche(20) && contientTous(a,set));
    }
    public static void testVerifieNaifA() {
        ABR a = construitArbre1();

        printTest("testVerifieNaifA", a.verifieNaive());
    }

    public static void testVerifieNaifB() {
        ABR a = construitArbre1();
        a.getFilsD().getFilsG().setVal(9);//plus un abr
        printTest("testVerifieNaifB", !a.verifieNaive());
    }


    public static void testVerifieV1A() {
        ABR a = construitArbre1();

        printTest("testVerifieV1A", a.verifieV1());
    }

    public static void testVerifieV1B() {
        ABR a = construitArbre1();
        a.getFilsD().getFilsG().setVal(9);//plus un abr
        printTest("testVerifieV1B", !a.verifieV1());
    }

    public static void testVerifieV2A() {
        ABR a = construitArbre1();

        printTest("testVerifieV2A", a.verifieV2());
    }

    public static void testVerifieV2B() {
        ABR a = construitArbre1();
        a.getFilsD().getFilsG().setVal(9);//plus un abr
        printTest("testVerifieV2B", !a.verifieV2());
    }


    public static void main(String[] args){
        ABR a0 = construitArbre0();
        System.out.println(a0);
        ABR a = construitArbre1();
        System.out.println(a);
        testEgal5();
        testRecherche();
        testSuppr1();
        testSuppr2();
        testSuppr3();
        testVerifieNaifA();
        testVerifieNaifB();
        testVerifieV1A();
        testVerifieV1B();
        testVerifieV2A();
        testVerifieV2B();

    }

}
